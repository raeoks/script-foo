def balSumSlice(array):
  """
  naive method as given in CLRS
  input: integer array (ex. [1,2,4])
  output: if found a slice of given array 'a' at index 'i' such that sum(a[:i]) == sum(a[i:]) then returns a[1:] otherwise -1
  """
  rightSum = sum(array)
  leftSum = 0
  for index in range(len(array)):
    if leftSum == rightSum:
      return array[:index]
    leftSum += array[index]
    rightSum -= array[index]
  return -1

def test():
  print "----- balance sum slice ----- (testing)"
  array1 = [1,2,3,1,6,1]
  array2 = []
  array3 = [1,2,3,4,5,6]
  output1 = [1,2,3,1]
  output2 = -1
  output3 = -1
  call1 = balSumSlice(array1)
  call2 = balSumSlice(array2)
  call3 = balSumSlice(array3)
  if call1 == output1:
    print "---- TEST1 Passed ----"
  else:
    print "---- TEST1 Failed ----"    
  if call2 == output2:
    print "---- TEST2 Passed ----"
  else:
    print "---- TEST2 Failed ----"    
  if call3 == output3:
    print "---- TEST3 Passed ----"
  else:
    print "---- TEST3 Failed ----"    

def main():
  print "script-foo"
  test()  
  
if __name__=="__main__":
  main()
