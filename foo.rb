def split(arr, index = 0, sum = 0)
 """recursive solution to balanced array split src: http://stackoverflow.com/questions/1132350/recursion-cut-array-of-integers-in-two-parts-of-equal-sum-in-a-single-pass"""
  curr = arr[index]
  return -1, curr if index+1 == arr.length
  sum = sum + curr
  i, tail = split(arr, index+1, sum)
  if i>-1
    return i, tail
  elsif sum == tail
    return index, sum
  end
  return -1, curr + tail
end

p split([1, 1, 2])
p split([1])
p split([-1,1,2])
p split([2,3,4])
p split([0,5,4,-9])
